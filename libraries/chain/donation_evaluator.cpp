/*
    Copyright (C) 2018
 */

#include <graphene/chain/database.hpp>
#include <graphene/chain/donation_evaluator.hpp>

namespace graphene {
namespace chain {

void_result
donation_create_evaluator::do_evaluate(const donation_create_operation &op) {
  try {
    // check whether the owner account exists
    FC_ASSERT(db().find_object(op.owner), "Owner account not exists");

    // check whether the donation payback asset exists and the balance
    // sufficient
    auto &asset_indx =
        db().get_index_type<asset_index>().indices().get<by_symbol>();
    auto asset_symbol_itr = asset_indx.find("DONA");
    FC_ASSERT(asset_symbol_itr != asset_indx.end());

    return void_result();
  }
  FC_CAPTURE_AND_RETHROW((op))
}

object_id_type
donation_create_evaluator::do_apply(const donation_create_operation &op) {
  try {

    database &d = db();

    // create donation object
    const auto &new_object =
        d.create<donation_object>([&](donation_object &obj) {
          obj.donor_name = op.donor_name;
          obj.donor_phone = op.donor_phone;
          obj.donor_id = op.donor_id;
          obj.funds = op.funds;
          obj.owner = op.owner;
          obj.create_date_time = op.create_date_time;
        });

    // create equal donation payback asset balance to donor
    // if the donor has donated before, accumlated the asset balance

    auto &asset_indx =
        d.get_index_type<asset_index>().indices().get<by_symbol>();
    auto asset_symbol_itr = asset_indx.find("DONA");

    asset_id_type assetid = asset_symbol_itr->get_id();

    auto &index = d.get_index_type<account_balance_index>()
                      .indices()
                      .get<by_account_asset>();
    auto itr = index.find(boost::make_tuple(op.owner, assetid));
    if (itr == index.end()) {
      d.create<account_balance_object>([&](account_balance_object &b) {
        b.owner = op.owner;
        b.asset_type = assetid;
        b.balance = op.funds;
      });
    } else {
      d.modify(*itr, [&](account_balance_object &b) {
        b.adjust_balance(asset(op.funds, assetid));
      });
    }

    return new_object.id;
  }
  FC_CAPTURE_AND_RETHROW((op))
}

} // namespace chain
} // namespace graphene
