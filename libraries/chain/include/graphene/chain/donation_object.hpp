/*
    Copyright (C) 2018
 */
#pragma once

#include <boost/multi_index/composite_key.hpp>
#include <graphene/chain/protocol/operations.hpp>
#include <graphene/db/generic_index.hpp>

namespace graphene {
namespace chain {

using namespace std;

class donation_object : public graphene::db::abstract_object<donation_object> {
   public:
    static const uint8_t space_id = protocol_ids;
    static const uint8_t type_id = donation_object_type;

    fc::string donor_phone;  // donor's phone number
    fc::string donor_name;   // donor's real name
    fc::string donor_id;     // donor's id card number
    share_type funds;        // donation amount

    // account will receive the equal value of token for this donation
    account_id_type owner;
    // donation time
    time_point_sec create_date_time;
};

struct by_id;
struct by_donor_phone;
struct by_donor_id;
struct by_donor_name;
struct by_funds;
typedef multi_index_container<
    donation_object,
    indexed_by<
        ordered_unique<tag<by_id>, member<object, object_id_type, &object::id>,
                       std::greater<object_id_type>>,

        ordered_non_unique<
            tag<by_donor_phone>,
            member<donation_object, fc::string, &donation_object::donor_phone>>,

        ordered_non_unique<
            tag<by_donor_id>,
            member<donation_object, fc::string, &donation_object::donor_id>>,

        ordered_non_unique<
            tag<by_funds>,
            composite_key<
                donation_object,
                member<donation_object, share_type, &donation_object::funds>,
                member<object, object_id_type, &object::id>>,
            composite_key_compare<std::greater<share_type>,
                                  std::less<object_id_type>>>,

        ordered_non_unique<
            tag<by_donor_name>,
            member<donation_object, fc::string, &donation_object::donor_name>>>>

    donation_multi_index_type;

/**
 * @ingroup object_index
 */
typedef generic_index<donation_object, donation_multi_index_type>
    donation_index;

}  // namespace chain
}  // namespace graphene

FC_REFLECT_DERIVED(
    graphene::chain::donation_object, (graphene::db::object),
    (donor_phone)(donor_name)(donor_id)(funds)(owner)(create_date_time))
