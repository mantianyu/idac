/*
    Copyright (C) 2018

 */
#pragma once
#include <graphene/chain/donation_object.hpp>
#include <graphene/chain/protocol/base.hpp>
#include <graphene/chain/protocol/types.hpp>

namespace graphene {
namespace chain {

struct donation_create_operation : public base_operation {
  struct fee_parameters_type {
    uint64_t fee = 0 * GRAPHENE_BLOCKCHAIN_PRECISION;
  };

  asset           fee;

  fc::string donor_phone; // donor's phone number
  fc::string donor_name;  // donor's real name
  fc::string donor_id;    // donor's id card number
  share_type funds;

  account_id_type owner;
  time_point_sec create_date_time;

  account_id_type fee_payer() const { return owner; }
  void validate() const {
      FC_ASSERT(!donor_phone.empty(), "Phone number must supply");
      FC_ASSERT(funds > 0, "Donation amount must > 0");
  }
  share_type calculate_fee(const fee_parameters_type &k) const { return k.fee; }
};

/*
struct donation_update_operation : public base_operation {
  struct fee_parameters_type {
    uint64_t fee = 0 * GRAPHENE_BLOCKCHAIN_PRECISION;
  };

  donation_id_type donation;

  optional<fc::string> donor_phone; // donor's phone number
  optional<fc::string> donor_name;  // donor's real name
  optional<fc::string> donor_id;    // donor's id card number
  optional<share_type> funds;

  optional<account_id_type> owner;
  optional<time_point_sec> create_date_time;

  account_id_type fee_payer() const { return owner; }
  void validate() const {
      FC_ASSERT(!donor_phone.empty(), "Phone number must supply")
      FC_ASSERT(funds.amount > 0, "Donation amount must > 0")
  }
  share_type calculate_fee(const fee_parameters_type &k) const { return k.fee; }
};
*/

} // namespace chain
} // namespace graphene

FC_REFLECT(graphene::chain::donation_create_operation::fee_parameters_type,
           (fee))
    /*
FC_REFLECT(graphene::chain::donation_update_operation::fee_parameters_type,
           (fee))
           */

FC_REFLECT(graphene::chain::donation_create_operation,
           (fee)(donor_phone)(donor_name)(donor_id)(funds)(owner)(create_date_time))
    /*
FC_REFLECT(
    graphene::chain::league_update_operation,
    (donor_phone)(donor_name)(donor_id)(funds)(owner)(create_date_time))
    */
